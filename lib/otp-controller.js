const totp = require('otplib/totp');
const crypto = require('crypto');
const fs = require('fs');
const lockData = JSON.parse(fs.readFileSync('config/lock_data.json', 'utf8'));

totp.options = { crypto };
totp.options = {
    digits: parseInt(process.env.TOTP_DIGITS),
    step: parseInt(process.env.TOTP_STEP)
}


function otpObject() {
    this.genToken = function(box) {
        if(this.secrets[box]) {
            return totp.generate(this.secrets[box]);
        } else {
            return false;
        }
        
    }
    this.verifyToken = function(token) {
        return totp.check(token, secret)
    }
    this.getBox = function(token) {
        for (var secret in this.secrets) {
            if(totp.check(token, this.secrets[secret])) {
                return secret;
            }
        }
        return false;
    }
    this.secrets = getSecrets()
}

function getSecrets() {
    var secrets = {}
    for (var lock in lockData) {
        secrets[lock] = eval("process.env.TOTP_SECRET_"+String(lock));
    }
    return secrets
}

module.exports = new otpObject