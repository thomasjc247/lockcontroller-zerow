const NodeCache = require('node-cache');
const NodeCacheOptions = { stdTTL: 0, checkperiod: 0 }
const fs = require('fs');
const lockData = JSON.parse(fs.readFileSync('config/lock_data.json', 'utf8'));
const statusCache = new NodeCache(NodeCacheOptions);

function cacheController() {
    this.set = function(key, value) {
      return statusCache.set(key, value);
    }
    this.get = function(key, value) {
      return statusCache.get(key);
    }
    this.lockData = JSON.parse(fs.readFileSync('config/lock_data.json', 'utf8'));
    for (var key in this.lockData) {
        this.set(key, lockData[key]["default"]);
    }
}

module.exports = new cacheController()