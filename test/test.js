const request = require('supertest');
const app = require('../app');
const assert = require('assert');

describe('API', function() {
  it('status correctly works', function(done) {
    request(app)
      .get('/api/status')
      .expect('{"1":{"name":"Box One","status":"locked"},"2":{"name":"Box Two","status":"locked"},"3":{"name":"Box Three","status":"locked"},"4":{"name":"Box Four","status":"locked"}}', done);
  });
  it('unlock rejects when no token is provided', function(done) {
    request(app)
      .post('/api/unlock')
      .expect({"code":403,"status":"Access Denied","error":"Invalid Token"}, done);
  });
  it('unlock rejects when no name is provided', function(done) {
    request(app)
      .post('/api/unlock')
      .send({token: 100000000})
      .expect('{"code":400,"status":"Bad Request","error":"Team Name Required"}', done);
  });
}); 
