var express = require('express');
var router = express.Router();
const cacheController = require('../lib/cache-controller');
const otpController = require('../lib/otp-controller')

function init() {
  console.log("Initialising NodeCache...");
  console.log(cacheController.get("1"));
}



/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/status', function(req, res, next) {
  var returnData = {} 
  for (var key in cacheController.lockData) {
    returnData[key] = {
      name: cacheController.lockData[key].name,
      status: cacheController.get(String(key))
    }
  }
  res.send(returnData)
});

router.get('/token', function(req, res, next) {
  if(!req.query.box) {
    res.send({code: 400, status: "Bad Request", error: "Requires Box Parameter"})
  } else {
    res.send({ code: otpController.genToken(req.query.box), box: req.query.box });
  }
  
});

router.post('/unlock', function(req, res, next) {
  if(!req.body.token) {
    res.status(403);
    res.send({code: 403, status: "Access Denied", error: "Invalid Token"})
  } else if(!req.body.name) {
    res.status(400);
    res.send({code: 400, status: "Bad Request", error: "Team Name Required"})
  } else {
    res.send(token)
    var token = req.body.token;
    var box = otpController.getBox(token);
    if(box == false) {
      res.send({code: 403, status: "Access Denied", error: "Invalid Token"})
    } else {
      // lockController.unlock(box);
      res.send({code: 200, box: box, status: "Command Acknowledged", action: "Unlocking Box "+box})
    }
  }
});

router.post('/lock', function(req, res, next) {
  if(!req.body.name) {
    res.send({code: 400, status: "Bad Request", error: "Team Name Required"})
  }
  if(!req.body.token) {
    res.status(403)
    res.send({code: 403, status: "Access Denied", error: "Invalid Token"})
  } else {
    var token = String(req.body.token);
    var box = otpController.getBox(token);
    if(box == false) {
      res.send({code: 403, status: "Access Denied", error: "Invalid Token"})
    } else {
      // lockController.lock(box);
      res.send({code: 200, box: box, status: "Command Acknowledged", action: "Locking Box "+box})
    }
  }
});

init();

module.exports = router;
