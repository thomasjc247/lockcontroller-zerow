# LockController 1.1.0
This version of LockController was developed for use on a Raspberry Pi Zero W. Running it on *anything* else will give you more speed.
[![pipeline status](https://gitlab.com/thomasjc247/lockcontroller-zerow/badges/master/pipeline.svg)](https://gitlab.com/thomasjc247/lockcontroller-zerow/commits/master) [![coverage report](https://gitlab.com/thomasjc247/lockcontroller-zerow/badges/master/coverage.svg)](https://gitlab.com/thomasjc247/lockcontroller-zerow/commits/master)
## API Docs
### Status
`GET /api/status` - Gets the current box status.
##### Example Data
```
{
    "1": {
        "name": "Box One",
        "status": "locked"
    }
}
```
---
### Get TOTP Code (Dev Only)
`GET /api/token?box=<box-number>` - Gets a token for the respective box, if enabled.
##### Example Data
```
{
    "code": 12345678,
    "box": 1
}
```
---
### Unlock Box
`POST /api/unlock` - Unlocks a box, requires parameters `token` and `name`.
##### Example Data
###### Happy Response
```
{
    "code": 200,
    "box": 1,
    "status": "Command Acknowledged",
    "action": "Unlocking Box 1"
}
```
###### Sad Responses
```
{
    "code": 403,
    "status": "Access Denied",
    "error": "Invalid Token"
}
```
```
{
    "code": 400,
    "status": "Bad Request",
    "error": "Team Name Required"
}
```
### Lock Box
`POST /api/lock` - Unlocks a box, requires parameters `token` and `name`.
##### Example Data
###### Happy Response
```
{
    "code": 200,
    "box": 1,
    "status": "Command Acknowledged",
    "action": "Locking Box 1"
}
```
###### Sad Responses
```
{
    "code": 403,
    "status": "Access Denied",
    "error": "Invalid Token"
}
```
```
{
    "code": 400,
    "status": "Bad Request",
    "error": "Team Name Required"
}
```
---
&copy; Thomas Judd-Cooper, 2018